const { expect } = require('chai');

const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const vesting = artifacts.require('VestingContract');
const token = artifacts.require("diamtoken");

contract('vesting', function (accounts) {

    // //  console.log(token.address)
    // console.log(vesting.address)

    beforeEach(async function () {
        // await token.approve(vesting.address, 100000, {from: accounts[0]});
        this.token = await token.new({ from: accounts[0] });
        this.vesting = await vesting.new(this.token.address, { from: accounts[0] });
    });

    // it('checks the  token address', async function () {
    //     expect(await this.vesting.token()).to.equal(this.token.address);
    //     //expect (await this.vesitng.token()).to.equal(contractAddress);
    // });

    // it('checks the  setBeneficiary function', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });
    //     const rec = await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] });
    //     expectEvent(rec, 'BeneficiarySet', {
    //         to: accounts[1],
    //         claim_id: "0",
    //         amount: "10000",
    //         // start_timee: unix.toString() ,
    //         // release_time : unix.toString() + "2592000"
    //     });
    // });

    // it('checks the tokens are debited from owner account to contract account after performing setBeneficiary function', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });
    //     await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] });

    //     let balanceOfVesting = await this.token.balanceOf(this.vesting.address);
    //     let balanceOfowner = await this.token.balanceOf(accounts[0]);
    //     expect(balanceOfVesting.toString()).to.equal("10000");
    //     expect(balanceOfowner.toString()).to.equal("9999999999999999999999990000");
    // });

    // it('cheks mutiple set and checks multiple release and if the users wallets are credited with tokens and vesting contract got debited', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });
    //     const rec1 = await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] });

    //     const rec2 = await this.vesting.setBeneficiary(accounts[2], 50000, { from: accounts[0] });

    //     expectEvent(rec1, 'BeneficiarySet', {
    //         to: accounts[1],
    //         claim_id: "0",
    //         amount: "10000",
    //     });
    //     expectEvent(rec2, 'BeneficiarySet', {
    //         to: accounts[2],
    //         claim_id: "1",
    //         amount: "50000",
    //     });

    //     await this.vesting.release(accounts[1], 0, 1, { from: accounts[0] });
    //     await this.vesting.release(accounts[2], 1, 1, { from: accounts[0] });

    //     let balanceOfVesting = await this.token.balanceOf(this.vesting.address);
    //     let balanceOfuser = await this.token.balanceOf(accounts[1]);
    //     let balanceOfuser1 = await this.token.balanceOf(accounts[2]);
    //     expect(balanceOfVesting.toString()).to.equal("54000");
    //     expect(balanceOfuser.toString()).to.equal("1000");
    //     expect(balanceOfuser1.toString()).to.equal("5000");
    // });

    // it('checks if reverts are done if called by other address other than owner', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });

    //     await expectRevert(
    //         this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[1] }),
    //         "Only owner can perform the operation",
    //     );

    //     const rec1 = await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] });

    //     await expectRevert(
    //         this.vesting.release(accounts[1], 0, 1, { from: accounts[1] }),
    //         "Only owner can perform the operation",
    //     );
    // });

    // it('checks if  releases are going well', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });

    //     const rec1 = await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] }); 10000

    //     await this.vesting.release(accounts[1], 0, 1, { from: accounts[0] });

    //     await this.vesting.release(accounts[1], 0, 2, { from: accounts[0] });

    //     let balanceOfVesting = await this.token.balanceOf(this.vesting.address);
    //     let balanceOfuser = await this.token.balanceOf(accounts[1]);

    //     expect(balanceOfVesting.toString()).to.equal("8900");
    //     expect(balanceOfuser.toString()).to.equal("1100");
    // });

    // it('checks if user cannot take released amounts again', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });

    //     const rec1 = await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] });

    //     await this.vesting.release(accounts[1], 0, 1, { from: accounts[0] });

    //     await expectRevert(
    //         this.vesting.release(accounts[1], 0, 1, { from: accounts[0] }),
    //         "Either you have overlapped or Already released !!"
    //     );

    //     let balanceOfVesting = await this.token.balanceOf(this.vesting.address);
    //     let balanceOfuser = await this.token.balanceOf(accounts[1]);

    //     expect(balanceOfVesting.toString()).to.equal("9000");
    //     expect(balanceOfuser.toString()).to.equal("1000");




    // });

    // it('checks if  revert is done if release are not in order', async function () {
    //     let unix = new Date();
    //     await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });

    //     const rec1 = await this.vesting.setBeneficiary(accounts[1], 10000, { from: accounts[0] }); 10000

    //     await this.vesting.release(accounts[1], 0, 1, { from: accounts[0] });

    //     await expectRevert(
    //         this.vesting.release(accounts[1], 0, 3, { from: accounts[0] }),
    //         "Either you have overlapped or Already released !!",
    //     );



    // });

    it('MASTER CHECK', async function () {
        let unix = new Date();
        await this.token.approve(this.vesting.address, 100000, { from: accounts[0] });

        const rec1 = await this.vesting.setBeneficiary(accounts[1], 50000, { from: accounts[0] });

        await this.vesting.release(accounts[1], 0, 1, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 2, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 3, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 4, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 5, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 6, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 7, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 8, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 9, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 10, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 11, { from: accounts[0] });
        await this.vesting.release(accounts[1], 0, 12, { from: accounts[0] });
        // await this.vesting.release(accounts[1], 0, 1, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 2, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 3, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 4, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 5, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 6, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 7, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 8, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 9, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 10, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 11, { from: accounts[1] });
        // await this.vesting.release(accounts[1], 0, 12, { from: accounts[1] });

        //let balanceOfVesting = await this.token.balanceOf(this.vesting.address);
        let balanceOfuser = await this.token.balanceOf(accounts[1]);

        //expect(balanceOfVesting.toString()).to.equal("0");
        expect(balanceOfuser.toString()).to.equal("50000");

        await expectRevert(
                    this.vesting.release(accounts[1], 0, 12, { from: accounts[0] }),
                    "All the tokens are released to the wallet",
                );



    });

});



