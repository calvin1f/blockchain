pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
//import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract VestingContract is Context, Ownable {
    using SafeERC20 for IERC20;
    //using SafeMath for uint256;

    address[] current_beneficiaries;

    IERC20 private immutable _token;

    uint256 lock_duration = 31556926; //duration set to 12 months  //"test": "test"

    //uint256 rate;

    constructor(IERC20 token_) public {
        _token = token_;
    }

    struct VestingDetails {
        uint256 id;
        address beneficiary;
        uint256 amount;
        uint256 start_time;
        uint256 release_time;
        uint256 claimed_at;
        bool status;
        bool released;
        uint256 a1;
    }

    struct BeneficiaryDetails {
        uint256[] active_ids;
        uint256[] released_ids;
        uint256 count;
        uint256 released;
    }

    VestingDetails[] public vesting;

    mapping(address => BeneficiaryDetails) public beneficiary_details;

    event BeneficiarySet(
        address indexed to,
        uint256 claim_id,
        uint256 amount,
        uint256 start_timee,
        uint256 release_time
    );

    // event Released(
    //     address indexed from,
    //     uint256 claim_id,
    //     uint256 locked_amount,
    //     uint256 claimed_amount,
    //     uint256 claimed_at
    // );

    // function setReleaseRate(uint256 _rate) public returns (bool) {
    //     rate = _rate;
    //     return true;
    // }

    function token() public view virtual returns (IERC20) {
        return _token;
    }

    function setBeneficiary(address beneficiary, uint256 amount) public {
        uint256 start_time = block.timestamp;
        uint256 end = start_time + lock_duration;
        uint256 length = vesting.length;
        token().safeTransferFrom(_msgSender(), address(this), amount);
        vesting.push(
            VestingDetails(
                length,
                beneficiary,
                amount,
                start_time,
                end,
                0,
                true,
                false,
                0            )
        );
        beneficiary_details[beneficiary].active_ids.push(length);
        beneficiary_details[beneficiary].count++;
        current_beneficiaries.push(beneficiary);
        emit BeneficiarySet(beneficiary, length, amount, start_time, end);
    }

    function release(uint256 id) public returns(bool) {
        VestingDetails memory vesting_details = vesting[id];
        require(
            vesting_details.beneficiary == _msgSender(),
            "Invalid Beneficiary"
        );
        require(
            block.timestamp < (vesting_details.start_time + 30 days),
            "Withdraw option is not started yet"
        );
        uint256 diff = vesting_details.start_time - block.timestamp; //3
        uint256 du = diff / 2592000; //3        //du is the month he is interacting with the contract since started vesting
        //uint256 a2 = vesting_details.a1;
        uint256 last = vesting_details.a1;//vesting_details.a1(a2 - 1); //this stores the last month out of 12 months he withdrawed
        uint256 claimed_amount = 0;

        if (last == 0) {
            if (du == 1) {
                //2592000 is 30 days
                claimed_amount = 10 % vesting_details.amount;
                d2(id, 1, claimed_amount);
            } else if (du == 2) {
                claimed_amount = 10+1 % vesting_details.amount;  
                d2(id, 2, claimed_amount);
            } else if (du == 3) {
                claimed_amount = 10+1+3 % vesting_details.amount;
                d2(id, 3, claimed_amount);
            } else if (du == 4) {
                claimed_amount = 10+1+3+4 % vesting_details.amount;
                d2(id, 4, claimed_amount);
            } else if (du == 5) {
                claimed_amount = 10+1+3+4+5 % vesting_details.amount;
                d2(id, 5, claimed_amount);
            } else if (du == 6) {
                claimed_amount =10+1+3+4+5+7 % vesting_details.amount;
                d2(id, 6, claimed_amount);
            } else if (du == 7) {
                claimed_amount = 10+1+3+4+5+7+8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount = 10+1+3+4+5+7+8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 10+1+3+4+5+7+8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 0+1+3+4+5+7+8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 0+1+3+4+5+7+8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 0+1+3+4+5+7+8+10+11+12+14+5 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        /////////////////
        else if (last == 1) {
            if (du == 2) {
                claimed_amount = 10+1 % vesting_details.amount;  
                d2(id, 2, claimed_amount);
            } else if (du == 3) {
                claimed_amount = 10+1+3 % vesting_details.amount;
                d2(id, 3, claimed_amount);
            } else if (du == 4) {
                claimed_amount = 10+1+3+4 % vesting_details.amount;
                d2(id, 4, claimed_amount);
            } else if (du == 5) {
                claimed_amount = 10+1+3+4+5 % vesting_details.amount;
                d2(id, 5, claimed_amount);
            } else if (du == 6) {
                claimed_amount =10+1+3+4+5+7 % vesting_details.amount;
                d2(id, 6, claimed_amount);
            } else if (du == 7) {
                claimed_amount = 10+1+3+4+5+7+8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount = 10+1+3+4+5+7+8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 10+1+3+4+5+7+8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 10+1+3+4+5+7+8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 10+1+3+4+5+7+8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 10+1+3+4+5+7+8+10+11+12+14+5 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 2) {
            if (du == 3) {
                claimed_amount = 3 % vesting_details.amount;
                d2(id, 3, claimed_amount);
            } else if (du == 4) {
                claimed_amount = 3+4 % vesting_details.amount;
                d2(id, 4, claimed_amount);
            } else if (du == 5) {
                claimed_amount = 3+4+5 % vesting_details.amount;
                d2(id, 5, claimed_amount);
            } else if (du == 6) {
                claimed_amount =3+4+5+7 % vesting_details.amount;
                d2(id, 6, claimed_amount);
            } else if (du == 7) {
                claimed_amount =3+4+5+7+8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount =3+4+5+7+8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount =3+4+5+7+8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount =3+4+5+7+8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 3+4+5+7+8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 3+4+5+7+8+10+11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 3) {
            if (du == 4) {
                claimed_amount = 4 % vesting_details.amount;
                d2(id, 4, claimed_amount);
            } else if (du == 5) {
                claimed_amount = 4+5 % vesting_details.amount;
                d2(id, 5, claimed_amount);
            } else if (du == 6) {
                claimed_amount =4+5+7 % vesting_details.amount;
                d2(id, 6, claimed_amount);
            } else if (du == 7) {
                claimed_amount = 4+5+7+8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount = 4+5+7+8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 4+5+7+8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 4+5+7+8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 4+5+7+8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 0+1+3+4+5+7+8+10+11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 4) {
            if (du == 5) {
                claimed_amount = 5 % vesting_details.amount;
                d2(id, 5, claimed_amount);
            } else if (du == 6) {
                claimed_amount =5+7 % vesting_details.amount;
                d2(id, 6, claimed_amount);
            } else if (du == 7) {
                claimed_amount = 5+7+8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount = 5+7+8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 5+7+8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 5+7+8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 5+7+8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 5+7+8+10+11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 5) {
            if (du == 6) {
                claimed_amount =7 % vesting_details.amount;
                d2(id, 6, claimed_amount);
            } else if (du == 7) {
                claimed_amount = 7+8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount = 7+8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 7+8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 7+8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 7+8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 7+8+10+11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 6) {
            if (du == 7) {
                claimed_amount = 8 % vesting_details.amount;
                d2(id, 7, claimed_amount);
            } else if (du == 8) {
                claimed_amount = 8+10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 8+10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 8+10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 8+10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 8+10+11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 7) {
            if (du == 8) {
                claimed_amount = 10 % vesting_details.amount;
                d2(id, 8, claimed_amount);
            } else if (du == 9) {
                claimed_amount = 10+11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 10+11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 10+11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 10+11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 8) {
           if (du == 9) {
                claimed_amount = 11 % vesting_details.amount;
                d2(id, 9, claimed_amount);
            } else if (du == 10) {
                claimed_amount = 11+12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 11+12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 11+12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 9) {
            if (du == 10) {
                claimed_amount = 12 % vesting_details.amount;
                d2(id, 10, claimed_amount);
            } else if (du == 11) {
                claimed_amount = 12+14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 12+14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 10) {
            if (du == 11) {
                claimed_amount = 14 % vesting_details.amount;
                d2(id, 11, claimed_amount);
            } else if (du == 12) {
                claimed_amount = 14+15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
        //////////////////////////////
        else if (last == 11) {
            if (du == 12) {
                claimed_amount = 15 % vesting_details.amount;
                d2(id, 12, claimed_amount);
            }
        }
    return _releaseWithAmount(id, claimed_amount);

    }

    function d2(uint id, uint o, uint256 ca) internal {
         VestingDetails memory vesting_details = vesting[id];
         vesting_details.amount -= ca;
         vesting_details.a1=o;
    }

    function _releaseWithAmount(
        uint256 id,
        uint256 claimed_amount
    ) internal returns (bool) {
        if (claimed_amount > 0) {
            address sender = _msgSender();
            token().safeTransfer(sender, claimed_amount);
            // if (owner_amount > 0) {
            //     token().safeTransfer(owner(), owner_amount);
            // }
            vesting[id].claimed_at = block.timestamp;
            vesting[id].status = false;
            vesting[id].released = true;
            beneficiary_details[sender].released++;
            beneficiary_details[sender].released_ids.push(id);
           // emit Released(sender, id, claimed_amount, block.timestamp);
           return true;
        }
        return false;
    }

    // function release(bool force) public {
    //     bool state = force;
    //     for (uint256 i; i < beneficiary_details[msg.sender].active_ids.length; i++) {
    //         uint256 id = beneficiary_details[msg.sender].active_ids[i];
    //         releaseById(id, force);
    //     }
    // }

    // function releaseById(uint256 id, bool force) public returns (bool) {
    //     VestingDetails memory vesting_details = vesting[id];
    //     require(vesting_details.beneficiary == _msgSender(), "Invalid Beneficiary");
    //     if(vesting_details.released == false) {
    //         uint256 amount = vesting_details.amount;
    //         uint256 claimed_amount = 0;
    //         uint256 owner_amount = 0;
    //         if (vesting_details.release_time > block.timestamp) {
    //             if(force == true) {
    //                 claimed_amount = amount.div(2);
    //                 owner_amount = claimed_amount;
    //             }
    //         }
    //         if(vesting_details.release_time < block.timestamp) {
    //             claimed_amount = amount;
    //         }
    //         return _releaseWithAmount(id, amount, claimed_amount, owner_amount);
    //     }
    //     return false;
    // }

    // function _releaseWithAmount(uint256 id, uint256 amount, uint256 claimed_amount, uint256 owner_amount) private returns (bool) {
    //     if(claimed_amount > 0) {
    //         address sender = _msgSender();
    //         token().safeTransfer(sender, claimed_amount);
    //         if(owner_amount > 0) {
    //             token().safeTransfer(owner(), owner_amount);
    //         }
    //         vesting[id].claimed_at = block.timestamp;
    //         vesting[id].status = false;
    //         vesting[id].released = true;
    //         beneficiary_details[sender].released ++;
    //         beneficiary_details[sender].released_ids.push(id);
    //         emit Released(sender, id, amount, claimed_amount, block.timestamp);
    //         return true;
    //     }
    //     return false;
    // }
}



/*    BACK UP
function release(uint256 id) public {
        VestingDetails memory vesting_details = vesting[id];
        require(
            vesting_details.beneficiary == _msgSender(),
            "Invalid Beneficiary"
        );
        require(
            block.timestamp <= (vesting_details.start_time + 30 days),
            "Withdraw option is not started yet"
        );
        uint256 diff = vesting_details.start_time - block.timestamp;
        uint256 du = div(diff, 2592000);
        uint256 a2 = vesting_details.a1.length;
        uint256 last = vesting_details.a1(a2 - 1);
        uint256 claimed_amount = 0;

        if (last == 0) {
            if (du == 1) {
                //2592000 is 30 days
                claimed_amount = 10.00000 % vesting_details.amount;

                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(1);
            } else if (du == 2) {
                claimed_amount = 1.36363 %  vesting_details.amount;
                d2(id, 2);
            } else if (du == 3) {
                claimed_amount = 2.727273 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(3);
            } else if (du == 4) {
                claimed_amount = 4.090909 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(4);
            } else if (du == 5) {
                claimed_amount = 5.454545 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(5);
            } else if (du == 6) {
                claimed_amount = mod(vesting_details.amount, 6.818182);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(6);
            } else if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        /////////////////
        else if (last == 1) {
            if (du == 2) {
                claimed_amount = 1.36363 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(2);
            } else if (du == 3) {
                claimed_amount = 2.727273 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(3);
            } else if (du == 4) {
                claimed_amount = 4.090909 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(4);
            } else if (du == 5) {
                claimed_amount = mod(vesting_details.amount, 5.454545);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(5);
            } else if (du == 6) {
                claimed_amount = mod(vesting_details.amount, 6.818182);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(6);
            } else if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 2) {
            if (du == 3) {
                claimed_amount = 2.727273 %  vesting_details.amount;
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(3);
            } else if (du == 4) {
                claimed_amount = mod(vesting_details.amount, 4.090909);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(4);
            } else if (du == 5) {
                claimed_amount = mod(vesting_details.amount, 5.454545);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(5);
            } else if (du == 6) {
                claimed_amount = mod(vesting_details.amount, 6.818182);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(6);
            } else if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 3) {
            if (du == 4) {
                claimed_amount = mod(vesting_details.amount, 4.090909);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(4);
            } else if (du == 5) {
                claimed_amount = mod(vesting_details.amount, 5.454545);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(5);
            } else if (du == 6) {
                claimed_amount = mod(vesting_details.amount, 6.818182);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(6);
            } else if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 4) {
            if (du == 5) {
                claimed_amount = mod(vesting_details.amount, 5.454545);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(5);
            } else if (du == 6) {
                claimed_amount = mod(vesting_details.amount, 6.818182);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(6);
            } else if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 5) {
            if (du == 6) {
                claimed_amount = mod(vesting_details.amount, 6.818182);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(6);
            } else if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 6) {
            if (du == 7) {
                claimed_amount = mod(vesting_details.amount, 8.181818);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(7);
            } else if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 7) {
            if (du == 8) {
                claimed_amount = mod(vesting_details.amount, 9.545455);
                vesting_details.a1.push(8);
            } else if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 8) {
            if (du == 9) {
                claimed_amount = mod(vesting_details.amount, 10.909091);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(9);
            } else if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 9) {
            if (du == 10) {
                claimed_amount = mod(vesting_details.amount, 12.272727);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(10);
            } else if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 10) {
            if (du == 11) {
                claimed_amount = mod(vesting_details.amount, 13.636364);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(11);
            } else if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
        //////////////////////////////
        else if (last == 11) {
            if (du == 12) {
                claimed_amount = mod(vesting_details.amount, 15.000000);
                vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(12);
            }
        }
    }

    function d2(uint id, uint o) internal {
         VestingDetails memory vesting_details = vesting[id];
         vesting_details.amount -= claimed_amount;
                vesting_details.a1.push(o);
    }
 */