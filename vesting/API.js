const express = require('express');
//const InputDataDecoder = require('ethereum-input-data-decoder');

var router = express.Router();

const Web3 = require('web3');

const app = express();

console.log("good")

const web3 = new Web3('https://ropsten.infura.io/v3/0186bbd47475436b9e3ff3d644b4d21c');

const abi = [{ "inputs": [], "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "owner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "spender", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "value", "type": "uint256" }], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "previousOwner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "newOwner", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "to", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "value", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "inputs": [{ "internalType": "address", "name": "owner", "type": "address" }, { "internalType": "address", "name": "spender", "type": "address" }], "name": "allowance", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "approve", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "account", "type": "address" }], "name": "balanceOf", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "decimals", "outputs": [{ "internalType": "uint8", "name": "", "type": "uint8" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "subtractedValue", "type": "uint256" }], "name": "decreaseAllowance", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "addedValue", "type": "uint256" }], "name": "increaseAllowance", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "name", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "owner", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "renounceOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "symbol", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "totalSupply", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "recipient", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "transfer", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "sender", "type": "address" }, { "internalType": "address", "name": "recipient", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }]
const abi2 = [{ "inputs": [{ "internalType": "contract IERC20", "name": "token", "type": "address" }], "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "to", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "claim_id", "type": "uint256" }, { "indexed": false, "internalType": "uint256", "name": "amount", "type": "uint256" }, { "indexed": false, "internalType": "uint256", "name": "start_timee", "type": "uint256" }, { "indexed": false, "internalType": "uint256", "name": "release_time", "type": "uint256" }], "name": "BeneficiarySet", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "previousOwner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "newOwner", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "to", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "claim_id", "type": "uint256" }, { "indexed": false, "internalType": "uint256", "name": "amount", "type": "uint256" }, { "indexed": false, "internalType": "uint256", "name": "duration", "type": "uint256" }], "name": "released", "type": "event" }, { "inputs": [{ "internalType": "address", "name": "", "type": "address" }], "name": "beneficiary_details", "outputs": [{ "internalType": "uint256", "name": "count", "type": "uint256" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "owner", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "beneficiary", "type": "address" }, { "internalType": "uint256", "name": "id", "type": "uint256" }, { "internalType": "uint256", "name": "CurrentMonth", "type": "uint256" }], "name": "release", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "renounceOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "beneficiary", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "setBeneficiary", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "token", "outputs": [{ "internalType": "contract IERC20", "name": "", "type": "address" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "name": "vesting", "outputs": [{ "internalType": "uint256", "name": "id", "type": "uint256" }, { "internalType": "address", "name": "beneficiary", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }, { "internalType": "uint256", "name": "start_time", "type": "uint256" }, { "internalType": "uint256", "name": "release_time", "type": "uint256" }, { "internalType": "uint256", "name": "claimed_at", "type": "uint256" }, { "internalType": "bool", "name": "status", "type": "bool" }, { "internalType": "bool", "name": "released", "type": "bool" }, { "internalType": "uint256", "name": "nextPeriod", "type": "uint256" }], "stateMutability": "view", "type": "function" }]


const address = '0x46313b6231aD6650Aa20b3a7d4D9B8986277CEE5'
const adddress2='0xc15875c9b041129472C34163e86c92eb22189933';

const account1 = '0xb44Fb70aEF96C5aa1E463Aab9eFF5f596b13B013'
const account2 = '0x847ffb59a728ba5e3d530e4c2b843fa3c4403cfd'

//const decoder = new InputDataDecoder(abi)

let contract = new web3.eth.Contract(abi, address);
let contract2 = new web3.eth.Contract(abi, address);


private1 = 'fbb69ffabcad0e0a66ce6af6b199c3c9dd358223ec21eedd5c3b9ce66e61a9bf';

// ///////////////////////////////////////////////////////////////Token Contract API///////////////////////////////////////////////////////////////
function TransferERC20Token(toAddress, value) {
    return new Promise(function (resolve, reject) {
        try {
            web3.eth.getBalance(account1).then(function (bal) {
                console.log(account1)
                var balance = web3.utils.fromWei(bal, 'gwei');
                console.log(balance);
                web3.eth.getBlock("latest", false, (error, result) => {
                    var _gasLimit = result.gasLimit;
                    console.log(_gasLimit);
                    //gas limit
                    let contract = new web3.eth.Contract(abi, address);

                    contract.methods.decimals().call({ from: account1 }).then(function (result) {
                        try {
                            var decimals = result;
                            console.log(value);
                                let amount1 = web3.utils.toWei(value.toString(), 'ether');
                        var amount = web3.utils.toBN(amount1);

                            console.log(decimals);
                          //  let amount = parseFloat(value) * Math.pow(10, decimals);
                            //getting correct amt
                            web3.eth.getGasPrice(function (error, result) {
                                var _gasPrice = result;
                                console.log(_gasPrice);
                                if (balance >= _gasPrice) {

                                    console.log(account1);
                                    try {
                                        const Tx = require('ethereumjs-tx').Transaction; // require('@ethereumjs/tx').Transaction;
                                        const privateKey = Buffer.from(private1, 'hex')
                                        var _hex_gasLimit = web3.utils.toHex((_gasLimit + 1000000).toString());
                                        var _hex_gasPrice = web3.utils.toHex(_gasPrice.toString());
                                        var _hex_value = web3.utils.toHex(amount.toString());
                                        var _hex_Gas = web3.utils.toHex('60000');
                                        console.log(_hex_Gas);

                                        web3.eth.getTransactionCount(account1).then(
                                            nonce => {
                                                var _hex_nonce = web3.utils.toHex(nonce);
                                                console.log(_hex_nonce);

                                                const rawTx =
                                                {
                                                    nonce: _hex_nonce,
                                                    from: account1, //
                                                    to: address, //
                                                    gasPrice: _hex_gasPrice,
                                                    gasLimit: _hex_gasLimit,
                                                    gas: _hex_Gas,
                                                    value: '0x0',
                                                    data: contract.methods.transfer(toAddress, _hex_value).encodeABI()
                                                };
                                                console.log("catch");
                                                const tx = new Tx(rawTx, { chain: 'ropsten' });
                                                tx.sign(privateKey);
                                                //console.log(process.env.private1);
                                                var serializedTx = '0x' + tx.serialize().toString('hex');
                                                web3.eth.sendSignedTransaction(serializedTx, function (err, hash) {
                                                    if (err) {
                                                        reject(err);
                                                        console.log("catch3");
                                                    }
                                                    else {
                                                        resolve(hash);
                                                        console.log("Transaction hash " + hash);
                                                    }
                                                })
                                            });
                                    } catch (error) {
                                        reject(error);
                                    }
                                }
                                else {
                                    console.log("low ether balance to cover gas price")
                                }
                            });
                        } catch (error) {
                            reject(error);
                        }
                    });
                });
            })
        } catch (error) {
            reject(error);
        }
    })
}
var acc = '0x9e4a009D11DA92a1C66f7e0d9D40aFB1aF6865Df';
TransferERC20Token(acc,2)
// app.post('/transfer/:address2/:value', (req, res) => {
//     let a1 = account1;
//     let a2 = req.params.address2;
//     let v = req.params.value;
//     //let f = checkdiam(a1, v);
//     let valid = web3.utils.isAddress(a2);
//     if(valid){

//     contract.methods.balanceOf(a1).call().then(function (bal) {
//         let a = toString.bal;
//         let b = toString.v;
//       if(bal >= v){
//         TransferERC20Token(a1, a2, v);   //balance_of_sender);
//         res.status(200).send({status:"200"});
//       }
//       else{
//               res.status(400).send({status: "400"});

//       }
//     })}
//     else{
//            res.status(400).send({ Error: "Invalid address", status: "400"})
//    }
// })


// app.get('/validation/:address', (req, res) => {
// let a2 = req.params.address;
// let valid = web3.utils.isAddress(a2);
// if(valid){
//     res.status(200).send({status:"200"});
//   }
//   else{
//           res.status(400).send({ Error: "Invalid address", status: "400"});

//   }
// })



// app.get('/balance/:address', (req, res) => {
// let b;
// let valid = web3.utils.isAddress(req.params.address);
// if(valid){
// contract.methods.balanceOf(req.params.address).call()
//     .then(function (bal) {
//         console.log(bal)
//         b = web3.utils.fromWei(bal, 'ether')
//         if(b != null){
//             res.status(200).send({balance: b, status: "200" });
//         }
//         else{
//             res.status(404).send({ status: "404"});
//         }
//     }) }

//     else{
//         res.status(400).send({ Error: "Invalid address", status: "400"})
//     }
// })

// app.get('/transactionHistory/:address', (req, res) => {
// const trxi = {};
// const trxo = {};
// let a = 0;
// let b = 0;

// let valid = web3.utils.isAddress(req.params.address);
// if(valid){
// contract.getPastEvents('Transfer', {
//     filter: { to: req.params.address },
//     fromBlock: 0,
//     toBlock: 'latest'
// }, function (error, events) {
//     for (i = 0; i < events.length; i++) {
//         trxi[a] = { Hash: events[i].rtransactionHash, from: events[i].returnValues.from, to: events[i].returnValues.to, value: events[i].returnValues.value }
//         a++;
//     }
//     //console.log(trxi)

// }).then(function () {
//     contract.getPastEvents('Transfer', {
//         filter: { to: req.params.address },
//         fromBlock: 0,
//         toBlock: 'latest'
//     }, function (error, events) {

//         for (i = 0; i < events.length; i++) {
//             trxo[b] = { Hash: events[i].rtransactionHash, from: events[i].returnValues.from, to: events[i].returnValues.to, value: events[i].returnValues.value }
//             b++;
//         }
//         if(Object.keys(trxi).length == 0 && Object.keys(trxo).length == 0){
//             res.status(404).send({status: "404"})
//         }
//         else if(Object.keys(trxi).length == 0){
//             res.send({ express: trxo, status: "200"  })
//         }
//         else if(Object.keys(trxo).length == 0){
//             res.send({ express: trxi,status: "200" })
//         }
//         else{
//             res.send({ express: trxi, trxo ,status: "200"})
//         }

//         //console.log(trxi, trxo)
//     })

// }) }
// else{
//     res.status(400).send({  Error: "Invalid address",status: "400"});
// }
// })

// app.get('/AddressCreation', (req, res) => {
// let gen = web3.eth.accounts.create();
// let address = gen.address;
// let privateKey = gen.privateKey;
// let status= "200";
// let status4 = "404"

// if(address != null && privateKey != null){
//     res.status(200).send({address, privateKey, status})
// }
// else{
//     res.status(404).send({status4})
// }
// })

// // ///////////////////////////////////////////////////////////////Vestin Contract API///////////////////////////////////////////////////////////////

//  var user = {};


// function setBeneficiary(Address, value) {
//     var fromAddress = account1;
//     return new Promise(function (resolve, reject) {
//         try {
//             web3.eth.getBalance(fromAddress).then(function (bal) {
//                 var balance = web3.utils.fromWei(bal, 'gwei');
//                 console.log(balance);
//                 web3.eth.getBlock("latest", false, (error, result) => {
//                     var _gasLimit = result.gasLimit;
//                     console.log(_gasLimit);
//                     let contract2 = new web3.eth.Contract(abi2, adddress2);
//                    // tok_contract.methods.decimals().call({ from: account1 }).then(function (result) {
//                     try {
//                           //var decimals = result

//                         let amount1 = web3.utils.toWei(value.toString(), 'ether');
//                         var amount = web3.utils.toBN(amount1);
                        
//                         console.log(amount);
//                         web3.eth.getGasPrice(function (error, result) {
//                             var _gasPrice = result;
//                             console.log(_gasPrice);
//                             if (balance >= _gasPrice) {

//                                 console.log(account1);
//                                 try {
//                                     const Tx = require('ethereumjs-tx').Transaction; 
//                                     const privateKey = Buffer.from(private1, 'hex')

//                                     var _hex_gasLimit = web3.utils.toHex((_gasLimit + 1000000).toString());
//                                     var _hex_gasPrice = web3.utils.toHex(_gasPrice.toString());
//                                     var _hex_value = web3.utils.toHex(amount.toString());
//                                     var _hex_Gas = web3.utils.toHex('312803'); 
//                                     console.log(_hex_Gas); 

//                                     web3.eth.getTransactionCount(account1).then(
//                                         nonce => {
//                                             var _hex_nonce = web3.utils.toHex(nonce);
//                                             console.log(_hex_nonce);

//                                             const rawTx =
//                                             {
//                                                 nonce: _hex_nonce,
//                                                 from: fromAddress, //
//                                                 to: adddress2, //
//                                                 gasPrice: _hex_gasPrice,
//                                                 gasLimit: _hex_gasLimit,
//                                                 gas: _hex_Gas,
//                                                 value: '0x0',
//                                                 data: contract2.methods.setBeneficiary(Address, _hex_value).encodeABI()
//                                             };
//                                             console.log("catch");
//                                             const tx = new Tx(rawTx, { chain: 'ropsten' });
//                                             tx.sign(privateKey);
//                                             var serializedTx = '0x' + tx.serialize().toString('hex');
//                                             web3.eth.sendSignedTransaction(serializedTx, function (err, hash) {
//                                                 if (err) {
//                                                     reject(err);
//                                                     console.log("catch3");
//                                                 }
//                                                 else {
//                                                     resolve(hash);
//                                                     console.log("Transaction hash " + hash);
//                                                 }
//                                             })
//                                         });
//                                 } catch (error) {
//                                     reject(error);
//                                 }
//                             }
//                             else {
//                                 console.log("low ether balance to cover gas price")
//                             }
//                         });
//                     } 
//                      catch (error) {
//                         reject(error);
//                     }
//                // });
//                 });
//             })
//         } catch (error) {
//             reject(error);
//         }
//     })
// }


// //setBeneficiary(acc,20000)

// function release(Address, id, cm) {
//     var fromAddress = account1;
//     var ID = id;
//     var CurrentMonth = cm;
//     return new Promise(function (resolve, reject) {
//         try {
//                 web3.eth.getBlock("latest", false, (error, result) => {
//                     var _gasLimit = result.gasLimit;
//                     console.log(_gasLimit);
//                     let contract2 = new web3.eth.Contract(abi2, adddress2);
//                     try {
//                         web3.eth.getGasPrice(function (error, result) {
//                             var _gasPrice = result;
//                             console.log(_gasPrice);
//                                 console.log(account1);
//                                 try {
//                                     const Tx = require('ethereumjs-tx').Transaction; // require('@ethereumjs/tx').Transaction;
//                                     const privateKey = Buffer.from(private1, 'hex')

//                                     var _hex_gasLimit = web3.utils.toHex((_gasLimit + 1000000).toString());
//                                     var _hex_gasPrice = web3.utils.toHex(_gasPrice.toString());
//                                     var _hex_Gas = web3.utils.toHex('93544'); 
//                                     console.log(_hex_Gas);

//                                     web3.eth.getTransactionCount(account1).then(
//                                         nonce => {
//                                             var _hex_nonce = web3.utils.toHex(nonce);
//                                             console.log(_hex_nonce);

//                                             const rawTx =
//                                             {
//                                                 nonce: _hex_nonce,
//                                                 from: fromAddress, //
//                                                 to: adddress2, //
//                                                 gasPrice: _hex_gasPrice,
//                                                 gasLimit: _hex_gasLimit,
//                                                 gas: _hex_Gas,
//                                                 value: '0x0',
//                                                 data: contract2.methods.release(Address, ID, CurrentMonth).encodeABI()
//                                             };
//                                             console.log("catch");
//                                             const tx = new Tx(rawTx, { chain: 'ropsten' });
//                                             tx.sign(privateKey);
//                                             var serializedTx = '0x' + tx.serialize().toString('hex');
//                                             web3.eth.sendSignedTransaction(serializedTx, function (err, hash) {
//                                                 if (err) {
//                                                     reject(err);
//                                                     console.log("catch3");
//                                                 }
//                                                 else {
//                                                     resolve(hash);
//                                                     console.log("Transaction hash " + hash);
//                                                 }
//                                             })
//                                         });
//                                 } catch (error) {
//                                     reject(error);
//                                 }
//                         });
                    
//                     } 
                
                
//                      catch (error) {
//                         reject(error);
//                     }
//                 });
//         } catch (error) {
//             reject(error);
//         }
//     })
// }

// // release("0x847ffb59a728Ba5e3D530E4C2b843fa3C4403cFd",7,2);
// // var id = 0; //

// app.get('/setBeneficiary/:address/:value', async (req, res) => {
//     let a1 = req.params.address;
//     let v = req.params.value;
//     let valid = web3.utils.isAddress(a1);
//     if (valid && v != 0) {
//         await setBeneficiary(a1);
//         res.send({ set: "true", status: "200" });      
//         let ID = id;  
//         setObj(address,ID );
//         id ++;
//     }
//     else {
//         res.send({ Reason: "Invalid address", status: "400" });
//     }
// });

// var i = 1;

// function setObj(add, i) {
//         user[i] =  {address: add, id: i};
//         i++;
// }

// var size = Object.keys(user).length;

// app.post('/start', async (req, res) => {

//     var garbage = {};
//     var j = 0;
//     async function start(){
//     cron.schedule('0 0 1 * *',async function() {
//     for(i = 1; i <= size; i ++){

    
//         let current = user[i];
//         let address = current.address;
//         let id = current.id;

//     let valid = web3.utils.isAddress(address);
//     if (valid) {
//         await release(address, id) //current month get it from cron
        
//     }
//     else {
//          garbage[j] = {Address :address, ID: id} 
//         // res.send({ Reason: "Invalid address", status: "400" });
//     }

//  }

// }) }

//  start(); 
//  var Gsize = Object.keys(garbage).length;

//  res.send({  status: "200", releaseFails: garbage });
// })

// app.listen(3000, () => console.log('API for DIAM Token running on 3000'))


                                                                           
