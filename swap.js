const Web3 = require('web3');

const web3 = new Web3('https://ropsten.infura.io/v3/0186bbd47475436b9e3ff3d644b4d21c');

const abi = [{"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"token0","type":"address"},{"indexed":true,"internalType":"address","name":"token1","type":"address"},{"indexed":false,"internalType":"address","name":"pair","type":"address"},{"indexed":false,"internalType":"uint256","name":"","type":"uint256"}],"name":"PairCreated","type":"event"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"allPairs","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"allPairsLength","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"}],"name":"createPair","outputs":[{"internalType":"address","name":"pair","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"feeTo","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"feeToSetter","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"getPair","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeTo","type":"address"}],"name":"setFeeTo","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"name":"setFeeToSetter","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"}];

const address = '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f'

const account1 = '0xb44Fb70aEF96C5aa1E463Aab9eFF5f596b13B013'
const account2 = '0x847ffb59a728ba5e3d530e4c2b843fa3c4403cfd'

//const decoder = new InputDataDecoder(abi)

let contract = new web3.eth.Contract(abi, address);


private1 = 'fbb69ffabcad0e0a66ce6af6b199c3c9dd358223ec21eedd5c3b9ce66e61a9bf';


console.log(contract)
//const tx = contract.methods.createPair('0x46313b6231ad6650aa20b3a7d4d9b8986277cee5','0x0a180a76e4466bf68a7f86fb029bed3cccfaaac5'); //first one is DIAM and second is WETH

//const encodedABI = tx.encodeABI();


// function sendSigned(txData, cb) {
//     const Tx = require('ethereumjs-tx').Transaction;
//     //const Tx = require('ethereumjs-tx');
//     const privateKey = Buffer.from(private1, 'hex')
//     const transaction = new Tx(txData, { chain: 'ropsten'});
//     //const transaction = new Tx(txData)
//     transaction.sign(privateKey)
//     const serializedTx = transaction.serialize().toString('hex')
//     web3.eth.sendSignedTransaction('0x' + serializedTx, cb)
//   }
//   // get the number of transactions sent so far so we can create a fresh nonce
//   web3.eth.getTransactionCount(account1).then(txCount => {
//     // construct the transaction data
//     const txData = {
//       nonce: web3.utils.toHex(txCount),
//       gasLimit: web3.utils.toHex(6000000),
//       gasPrice: web3.utils.toHex(10000000000),
//       to: address,
//       from: account1,
//       data: encodedABI
//     }
//     // fire away!
//     sendSigned(txData, function(err, result) {
//       if (err) return console.log('error', err)
//       console.log('sent', result)  //0xdd46dacb2d1662b667cb8c4ed9ef3a94ef7b322016233f1dfbd03f447dc63c00 <= transaction hash of pair creation
//     })  
//   })

// const tx = contract.methods.getPair('0x46313b6231ad6650aa20b3a7d4d9b8986277cee5','0x0a180a76e4466bf68a7f86fb029bed3cccfaaac5');
// console.log(tx)



