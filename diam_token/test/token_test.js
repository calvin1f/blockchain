const diamtoken = artifacts.require("diamtoken");
const diamsale = artifacts.require("diamsale");

contract("diamtoken", function (accounts) {

  let instance;
  let cd = instance;
  let token_name = "DIAM TOKENS";
  let token_symbol = "DIAM";
  var owner = accounts[0];
  var buyer = accounts[1];
  let alpha = accounts[2];
  let dolores = accounts[3];
  let contract = diamtoken.address;
  let total_supply = "10000000000000000000000000000";
  let buy = "10000000000000000000";

  beforeEach(async function () {
    try {
      instance = await diamtoken.new({ from: owner });
      sale = await diamsale.new(instance.address, 18, {from: owner})
    } catch (err) {
      console.log(err.message);
    }
  });

  // it("check the name", async function () {
  //   let name = await instance.name();
  //   return assert.equal(name, token_name, "not correct name");
  // });

  // it("check the symbol", async function () {
  //   let symbol = await instance.symbol();
  //   return assert.equal(symbol, token_symbol, "not correct symbol");
  // });

  // it("check the total supply", async function () {
  //   let supply = await instance.totalSupply();
  //   return assert.equal(supply, total_supply, "not correct supply");
  // });

    it("check the purchase", async function () {
    await instance.transfer(sale.address, total_supply, { from: owner });
    await sale.get(buy, { from: dolores });
    // let allowance_of_contract = await instance.allowance(owner,sale.address);
    // return assert.equal(allowance_of_contract.toString(), total_supply, "not correct supply");
    // await sale.purchase("10", {from: buyer})
    let balance_of_buyer = await instance.balanceOf(dolores);
    return assert.equal(balance_of_buyer.toString(), buy, "pruchase not ok");
  });

  // it("check the purchase", async function () {
  //   await instance.get(sale.address, total_supply, { from: owner });
  //   // let allowance_of_contract = await instance.allowance(owner,sale.address);
  //   // return assert.equal(allowance_of_contract.toString(), total_supply, "not correct supply");
  //   // await sale.purchase("10", {from: buyer})
  //   let balance_of_buyer = await instance.balanceOf(sale.address);
  //   return assert.equal(balance_of_buyer.toString(), total_supply, "pruchase not ok");
  // });

  //transfers
  // it("check the transferfrom", async function () {
  //   //var cont = instance.address;
  //   await instance.approve(dolores, total_supply, { from: owner });
  //   // let allowance_of_contract = await instance.allowance(owner,dolores);
  //   // return assert.equal(allowance_of_contract.toString(), total_supply, "not correct supply");
  //   await instance.transferFrom(owner, buyer, "100000000000000000000", { from: dolores })
  //   let balance_of_buyer = await instance.balanceOf(buyer);
  //   return assert.equal(balance_of_buyer.toString(), "100000000000000000000", "pruchase not ok");
  // });

  // // transfer from ans check
  // it("check the total supply", async function () {
  //   await instance.transfer(buyer, "100000000000000000000", { from: owner })
  //   let balance_of_buyer = await instance.balanceOf(buyer);
  //   return assert.equal(balance_of_buyer.toString(), "100000000000000000000", "pruchase not ok");
  // });


});
